package com.sky.fatherbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FatherbankApplication {

    public static void main(String[] args) {
        SpringApplication.run(FatherbankApplication.class, args);
    }

}
